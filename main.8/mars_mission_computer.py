import json
import platform
import psutil

class MissionComputer:
    def get_mission_computer_info(self):
        # 운영체제 정보
        os_info = platform.uname()
        os_name = os_info.system
        os_version = os_info.version
        
        # CPU 정보
        cpu_info = platform.processor()
        cpu_cores = psutil.cpu_count(logical=True)
        
        # 메모리 정보
        memory_info = psutil.virtual_memory()
        memory_size = memory_info.total
        
        # JSON 형식으로 출력
        info_dict = {
            "운영체제": os_name,
            "운영체제 버전": os_version,
            "CPU 타입": cpu_info,
            "CPU 코어 수": cpu_cores,
            "메모리 크기": memory_size
        }
        print(json.dumps(info_dict, indent=4))

    def get_mission_computer_load(self):
        # CPU 실시간 사용량
        cpu_percent = psutil.cpu_percent(interval=1)
        
        # 메모리 실시간 사용량
        memory_percent = psutil.virtual_memory().percent
        
        # JSON 형식으로 출력
        load_dict = {
            "CPU 사용량": cpu_percent,
            "메모리 사용량": memory_percent
        }
        print(json.dumps(load_dict, indent=4))

# MissionComputer 클래스 인스턴스 생성
runComputer = MissionComputer()

# 미션 컴퓨터의 시스템 정보 출력
print("미션 컴퓨터의 시스템 정보:")
runComputer.get_mission_computer_info()

# 미션 컴퓨터의 부하 정보 출력
print("\n미션 컴퓨터의 부하 정보:")
runComputer.get_mission_computer_load()
