import io
import os
from google.cloud import speech_v1p1beta1 as speech
import csv

def transcribe_audio(audio_file):
    client = speech.SpeechClient()

    with io.open(audio_file, "rb") as audio_file:
        content = audio_file.read()

    audio = speech.RecognitionAudio(content=content)
    config = speech.RecognitionConfig(
        encoding=speech.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=16000,
        language_code="ko-KR",
    )

    response = client.recognize(config=config, audio=audio)

    results = []
    for result in response.results:
        alternative = result.alternatives[0]
        results.append((alternative.transcript, alternative.confidence))

    return results

def save_transcription_to_csv(audio_file, transcriptions):
    csv_file = os.path.splitext(audio_file)[0] + ".csv"
    with open(csv_file, "w", newline="", encoding="utf-8") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["Time", "Transcription"])
        for i, (transcription, confidence) in enumerate(transcriptions):
            writer.writerow([f"00:{i:02}", transcription])

if __name__ == "__main__":
    audio_file = "your_audio_file.wav"  # 음성 파일 경로 입력
    transcriptions = transcribe_audio(audio_file)
    save_transcription_to_csv(audio_file, transcriptions)
