def caesar_cipher_decode(target_text):
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    decoded_results = []

    # 자리수에 따라 암호를 해독하여 리스트에 저장
    for shift in range(1, len(alphabet) + 1):
        decoded_text = ''
        for char in target_text:
            if char.upper() in alphabet:
                index = (alphabet.index(char.upper()) - shift) % len(alphabet)
                decoded_text += alphabet[index] if char.isupper() else alphabet[index].lower()
            else:
                decoded_text += char
        decoded_results.append(decoded_text)

    # 해독된 결과 출력
    for i, result in enumerate(decoded_results, 1):
        print(f"{i}. {result}")

# 테스트를 위한 암호문
encrypted_text = "B ehox Ftkl"

# 카이사르 암호를 해독하여 결과를 출력
caesar_cipher_decode(encrypted_text)
