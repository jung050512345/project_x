import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QGridLayout, QPushButton, QLineEdit
from PyQt5.QtGui import QFont

class Calculator(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Calculator")
        self.setGeometry(100, 100, 300, 400)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)

        self.display = QLineEdit()
        self.display.setReadOnly(True)
        self.layout.addWidget(self.display)

        self.buttons = [
            ('AC', (0, 0)),
            ('+/-', (0, 1)),
            ('%', (0, 2)),
            ('/', (0, 3)),
            ('7', (1, 0)),
            ('8', (1, 1)),
            ('9', (1, 2)),
            ('*', (1, 3)),
            ('4', (2, 0)),
            ('5', (2, 1)),
            ('6', (2, 2)),
            ('-', (2, 3)),
            ('1', (3, 0)),
            ('2', (3, 1)),
            ('3', (3, 2)),
            ('+', (3, 3)),
            ('0', (4, 0, 1, 2)),
            ('.', (4, 2)),
            ('=', (4, 3))
        ]

        self.grid_layout = QGridLayout()
        self.layout.addLayout(self.grid_layout)

        for button_text, pos in self.buttons:
            button = QPushButton(button_text)
            button.clicked.connect(lambda _, text=button_text: self.on_button_click(text))
            self.grid_layout.addWidget(button, *pos)
            self.setStyleSheet("background-color: black; color: white;")

    def on_button_click(self, text):
        if text == 'AC':
            self.display.clear()
        elif text in ('+', '-', '*', '/'):
            self.display.setText(self.display.text() + ' ' + text + ' ')
        elif text == '=':
            try:
                result = eval(self.display.text())
                # 소수점 6자리 이하로 반올림하여 출력
                result = round(result, 6)
                result_str = str(result)
                # 결과의 길이에 따라 폰트 크기 조정
                font_size = 20 if len(result_str) <= 10 else 14
                font = QFont()
                font.setPointSize(font_size)
                self.display.setFont(font)
                self.display.setText(result_str)
            except Exception as e:
                self.display.setText('Error')
        else:
            self.display.setText(self.display.text() + text)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    calc = Calculator()
    calc.show()
    sys.exit(app.exec_())


