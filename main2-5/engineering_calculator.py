import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QGridLayout, QPushButton, QLineEdit

class Calculator(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Calculator")
        self.setGeometry(100, 100, 300, 400)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)

        self.display = QLineEdit()
        self.display.setReadOnly(True)
        self.layout.addWidget(self.display)

        self.buttons = [
            ('(', (0, 0)),(')', (0, 1)),('mc', (0, 2)),('m+', (0, 3)),('m-', (0, 4)),
            ('mr', (0, 5)),('AC', (0,  6)),('+/-', (0, 7)),('%', (0, 8)),('/', (0, 9)),
            
            ('2nd', (1, 0)),('x2', (1, 1)),('x3', (1, 2)),('xy', (1, 3)),('ex', (1, 4)),
            ('10x', (1, 5)),('7', (1,  6)),('8', (1, 7)),('9', (1, 8)),('*', (1, 9)),
            
            ('1/x', (2, 0)),('2√x', (2, 1)),('3√x', (2, 2)),('y√x', (2, 3)),('in', (2, 4)),
            ('log10', (2, 5)),('4', (2,6)),('5', (2, 7)),('6', (2, 8)),('-', (2, 9)),
            
            ('x!', (3, 0)),('sin', (3, 1)),('cos', (3, 2)),('tan', (3, 3)),('e', (3, 4)),
            ('EE', (3, 5)),('1', (3,  6)),('2', (3, 7)),('3', (3, 8)),('+', (3, 9)),
            
            ('Rad', (4, 0)),('sinh', (4, 1)),('cosh', (4, 2)),('tanh', (4, 3)),('π', (4, 4)),
            ('Rand', (4, 5)),('0', (4,6,1,2)),('.', (4, 8)),('=', (4, 9)),
        ]

        self.grid_layout = QGridLayout()
        self.layout.addLayout(self.grid_layout)

        for button_text, pos in self.buttons:
            button = QPushButton(button_text)
            button.clicked.connect(lambda _, text=button_text: self.on_button_click(text))
            self.grid_layout.addWidget(button, *pos)

    def on_button_click(self, text):
        current_text = self.display.text()

        if text == 'C':
            self.display.clear()
        else:
            self.display.setText(current_text + text)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    calc = Calculator()
    calc.show()
    sys.exit(app.exec_())