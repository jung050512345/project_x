import zipfile
import time
import itertools

input_zipfile = 'emergency_storage_key.zip'
out_file = 'password.txt'

def unlock_zip(zipname) :
    cnt = 0
    start_time = time.time()
    candidates = [chr(i) for i in range(ord('a'), ord('z') + 1)] + \
                 [str(i) for i in range(10)]
    candidates_len = len(candidates)

    try :
        zfile = zipfile.ZipFile(zipname,'r')
        for pw_tuple in itertools.product(candidates, repeat=6):
            pw = "".join(pw_tuple)
            cnt += 1
            if zfile :
                try :
                    zfile.extractall(path='.',pwd=str(pw).encode('utf-8'))
                    print('YES',pw)
                    end_time = time.time()
                    elapsed_time = end_time - start_time
                    print(elapsed_time)
                    try :
                        fp = open(out_file,'w')
                        fp.write(pw)
                    except :
                        print('Fail To Open File')
                    return 1
                except Exception as e :
                    during_time = time.time() - start_time
                    print(f'Not Correct Password 시도 비밀번호 : {pw} 시행횟수 : {cnt} 수행시간 : {during_time:.5f}s')
                    pass

    except FileNotFoundError as e:
        print('No File In Current Pass')

unlock_zip(input_zipfile)
