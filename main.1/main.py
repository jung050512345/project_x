filename = 'main.1/mission_computer_main.log'

try:
    with open(filename, 'r') as file:
        
        log_entries = file.readlines()
    log_entries_sorted = sorted(log_entries[1:], key=lambda x: x.split(',')[0], reverse=True)

    
    print(log_entries[0].strip())
    for entry in log_entries_sorted:
        print(entry.strip())

except FileNotFoundError:
    print("File not found.")
