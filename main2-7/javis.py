import os
import pyaudio
import wave
from datetime import datetime

class AudioRecorder:
    def __init__(self):
        self.CHUNK = 1024
        self.FORMAT = pyaudio.paInt16
        self.CHANNELS = 1
        self.RATE = 44100
        self.audio = pyaudio.PyAudio()
        self.frames = []

    def start_recording(self):
        stream = self.audio.open(format=self.FORMAT,
                                 channels=self.CHANNELS,
                                 rate=self.RATE,
                                 input=True,
                                 frames_per_buffer=self.CHUNK)
        
        print("Recording...")
        self.frames = []
        while True:
            data = stream.read(self.CHUNK)
            self.frames.append(data)

            # 녹음 중지를 위해 키보드 입력 (예: q)을 받습니다.
            if input("Press q to stop recording: ").strip().lower() == 'q':
                break
        
        print("Recording finished.")
        stream.stop_stream()
        stream.close()

    def save_recording(self):
        now = datetime.now()
        timestamp = now.strftime("%Y%m%d-%H%M%S")
        filename = os.path.join("records", f"{timestamp}.wav")

        wf = wave.open(filename, 'wb')
        wf.setnchannels(self.CHANNELS)
        wf.setsampwidth(self.audio.get_sample_size(self.FORMAT))
        wf.setframerate(self.RATE)
        wf.writeframes(b''.join(self.frames))
        wf.close()

        print(f"Recording saved as {filename}")

if __name__ == "__main__":
    if not os.path.exists("records"):
        os.makedirs("records")

    recorder = AudioRecorder()
    recorder.start_recording()
    recorder.save_recording()
