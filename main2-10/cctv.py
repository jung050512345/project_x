import cv2
import os

# 준비
hog = cv2.HOGDescriptor() # 객체
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector()) # 모델 지정 
hogParams = {'winStride': (8,8), 'padding': (32,32), 'scale': 1.05, 'hitThreshold': 0} # 파라미터 설정 

# 이미지 파일 목록 가져오기
folder_path = 'main2/cctv'
image_files = [f for f in os.listdir(folder_path) if f.endswith(('.jpg', '.png', '.jpeg'))] 

# 현재 이미지 인덱스
current_index = 0

# 이미지 파일 읽기
def read_image(index):
    img_path = os.path.join(folder_path, image_files[index])
    return cv2.imread(img_path)

# 검출 및 출력 함수
def detect_and_show(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # 흑백 변환 
    human, r = hog.detectMultiScale(gray, **hogParams) # 사람 검출 - human에는 사람의 위치 정보가 들어감 

    if len(human) > 0: # 사람으로 인식한 게 1개 이상이라면 
        for (x, y, w, h) in human:
            cv2.rectangle(image, (x, y), (x+w, y+h), (255, 255, 255), 3) # 사각형을 그려! (여기서 255,255,255는 white 색)
        
    cv2.imshow('CCTV', image) # 출력 

# 첫 번째 이미지 표시
current_image = read_image(current_index)
detect_and_show(current_image)

while True:
    key = cv2.waitKey(0)
    
    if key == ord('\r'):  # 엔터를 누르면
        # 다음 이미지 읽기
        current_index += 1
        if current_index < len(image_files):
            current_image = read_image(current_index)
            detect_and_show(current_image)
        else:
            print("더 이상 이미지가 없습니다.")
            break

cv2.destroyAllWindows()
