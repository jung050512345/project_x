filename = 'Mars_Base_Inventory_List.csv'
binary_filename = 'Mars_Base_Inventory_List.bin'
inventory_list = []

try:
    # CSV 파일 열기
    with open(filename, 'r') as file:
        next(file)        
        for line in file:
            row = line.strip().split(',')
            try:
                row[2] = float(row[2]) 
                inventory_list.append(row)
            except ValueError:
                continue
    
    # 인화성 지수가 0.7 이상인 아이템만 추출하여 정렬
    filtered_items = [item for item in inventory_list if item[2] >= 0.7]
    sorted_items = sorted(filtered_items, key=lambda x: x[2], reverse=True)
    
    # 이진 파일로 저장
    with open(binary_filename, 'wb') as bin_file:
        for item in sorted_items:
            # 아이템의 세 번째 요소(float)를 문자열로 변환하여 쓰기
            item[2] = str(item[2]) 
            bin_file.write(','.join(item).encode() + b'\n')

    print(f"{binary_filename} 파일로 저장되었습니다.")

except FileNotFoundError:
    print("File not found.")

# 이진 파일 읽기
try:
    with open(binary_filename, 'rb') as bin_file:
        print(f"\n{binary_filename} 파일 내용:")
        for line in bin_file:
            print(line.decode().strip())

except FileNotFoundError:
    print(f"{binary_filename} 파일을 읽을 수 없습니다.")


