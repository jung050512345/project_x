import random
import json
import time
import multiprocessing
import platform  # platform 모듈 임포트
import psutil 
class MissionComputer:
    def __init__(self, name):
        self.name = name
        self.env_values = {
            'mars_base_internal_temperature': 0,
            'mars_base_external_temperature': 0,
            'mars_base_internal_humidity': 0,
            'mars_base_external_illuminance': 0,
            'mars_base_internal_co2': 0,
            'mars_base_internal_oxygen': 0
        }

    def get_sensor_data(self):
        while True:
            ds = DummySensor()
            ds.set_env()
            self.env_values = ds.env_values
            print(f"{self.name} 환경 정보:")
            print(json.dumps(self.env_values, indent=4))
            time.sleep(5)  # 5초마다 한 번씩 출력

    def get_mission_computer_info(self):
        while True:
            os_info = platform.uname()
            os_name = os_info.system
            os_version = os_info.version
            cpu_info = platform.processor()
            cpu_cores = psutil.cpu_count(logical=True)
            memory_info = psutil.virtual_memory()
            memory_size = memory_info.total
            info_dict = {
                "운영체제": os_name,
                "운영체제 버전": os_version,
                "CPU 타입": cpu_info,
                "CPU 코어 수": cpu_cores,
                "메모리 크기": memory_size
            }
            print(f"{self.name}의 시스템 정보:")
            print(json.dumps(info_dict, indent=4))
            time.sleep(20)  # 20초마다 한 번씩 출력

    def get_mission_computer_load(self):
        while True:
            cpu_percent = psutil.cpu_percent(interval=1)
            memory_percent = psutil.virtual_memory().percent
            load_dict = {
                "CPU 사용량": cpu_percent,
                "메모리 사용량": memory_percent
            }
            print(f"{self.name}의 부하 정보:")
            print(json.dumps(load_dict, indent=4))
            time.sleep(20)  # 20초마다 한 번씩 출력

class DummySensor:
    def __init__(self):
        self.env_values = {
            'mars_base_internal_temperature': 0,
            'mars_base_external_temperature': 0,
            'mars_base_internal_humidity': 0,
            'mars_base_external_illuminance': 0,
            'mars_base_internal_co2': 0,
            'mars_base_internal_oxygen': 0
        }

    def set_env(self):
        self.env_values['mars_base_internal_temperature'] = round(random.uniform(18, 30), 2)
        self.env_values['mars_base_external_temperature'] = round(random.uniform(0, 21), 2)
        self.env_values['mars_base_internal_humidity'] = round(random.uniform(50, 60), 2)
        self.env_values['mars_base_external_illuminance'] = round(random.uniform(500, 715), 2)
        self.env_values['mars_base_internal_co2'] = round(random.uniform(0.02, 0.1), 4)
        self.env_values['mars_base_internal_oxygen'] = round(random.uniform(4, 7), 2)

# 멀티 프로세스로 실행
if __name__ == "__main__":
    runComputer1 = MissionComputer("MissionComputer1")
    runComputer2 = MissionComputer("MissionComputer2")
    runComputer3 = MissionComputer("MissionComputer3")

    # 각각의 인스턴스를 별도의 프로세스로 실행
    p1 = multiprocessing.Process(target=runComputer1.get_sensor_data)
    p2 = multiprocessing.Process(target=runComputer1.get_mission_computer_info)
    p3 = multiprocessing.Process(target=runComputer1.get_mission_computer_load)

    p4 = multiprocessing.Process(target=runComputer2.get_sensor_data)
    p5 = multiprocessing.Process(target=runComputer2.get_mission_computer_info)
    p6 = multiprocessing.Process(target=runComputer2.get_mission_computer_load)

    p7 = multiprocessing.Process(target=runComputer3.get_sensor_data)
    p8 = multiprocessing.Process(target=runComputer3.get_mission_computer_info)
    p9 = multiprocessing.Process(target=runComputer3.get_mission_computer_load)

    # 프로세스 시작
    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    p6.start()
    p7.start()
    p8.start()
    p9.start()

    # 각 프로세스가 끝날 때까지 대기
    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()
    p8.join()
    p9.join()
