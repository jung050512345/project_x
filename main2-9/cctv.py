import os
import cv2

class ImageHelper:
    def __init__(self, folder_path):
        self.folder_path = folder_path
        self.image_files = [f for f in os.listdir(self.folder_path) if f.endswith(('.jpg', '.png', '.jpeg'))]
        self.num_images = len(self.image_files)
        self.current_index = 0

    def show_image(self, index):
        image_path = os.path.join(self.folder_path, self.image_files[index])
        image = cv2.imread(image_path)
        cv2.imshow('CCTV Viewer', image)

    def show_next_image(self):
        if self.current_index < self.num_images - 1:
            self.current_index += 1
            self.show_image(self.current_index)

    def show_previous_image(self):
        if self.current_index > 0:
            self.current_index -= 1
            self.show_image(self.current_index)

cctv_folder = "main2/cctv"
image_helper = ImageHelper(cctv_folder)

# 첫 번째 이미지 보기
image_helper.show_image(image_helper.current_index)

while True:
    key=cv2.waitKeyEx() # 키보드에서 입력키받음
    if key == 0x1B: #ESC키
        break
    elif key==0x270000: # 방향키 방향 전환 0x270000==right
        image_helper.show_next_image()
    elif key==0x250000: # 방향키 방향 전환 0x250000==left
        image_helper.show_previous_image()