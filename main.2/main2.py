filename = 'main.2/mission_computer_main.log'
log_entries = []

try:
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split(',')
            log_entry_dict = {'timestamp': parts[0], 'event': parts[1], 'message': parts[2]}
            log_entries.append(log_entry_dict)
    for entry in log_entries:
        print(entry)

except FileNotFoundError:
    print("File not found.")

