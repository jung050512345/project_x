import numpy as np
def read_csv_file(file_name):
    data = np.genfromtxt(file_name, delimiter=',', dtype=str, skip_header = 1)
    return data
def save_to_csv(file_name, data):
    try:
        np.savetxt(file_name, data, delimiter=',')
    except Exception as e:
        print(e)
arr1 = read_csv_file('mars_base_main_parts-001.csv')
arr2 = read_csv_file('mars_base_main_parts-002.csv')
arr3 = read_csv_file('mars_base_main_parts-003.csv')

arr2 = np.delete(arr2, 0, axis = 1)
arr3 = np.delete(arr3, 0, axis = 1)
parts = np.concatenate((arr1, arr2, arr3), axis = 1)

numeric_data = parts[:, 1:].astype(float)  # 숫자로 변환
sum_values = np.sum(numeric_data, axis=1)  # 각 행의 합 구하기
mean_values = sum_values / numeric_data.shape[1]  # 열 개수로 나눠서 평균 구하기

below_50_indices = np.where(mean_values < 50)
below_50_parts = parts[below_50_indices]


try:
    np.savetxt('parts_to_work_on.csv', below_50_parts, delimiter=',', fmt='%s')
except Exception as e:
    print(e)
try:
    parts2 = np.genfromtxt('parts_to_work_on.csv', delimiter=',', dtype=str)
    parts3 = np.transpose(parts2)
    print('----전치행렬----')
    print(parts3)
except Exception as e:
    print(e)

