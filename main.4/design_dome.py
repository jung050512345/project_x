total_dome_area = None
dome_weight = None

def sphere_area(diameter, material="glass", thickness=1):
    global total_dome_area, dome_weight
    
    density = {"glass": 2.4, "aluminum": 2.7, "carbon": 7.85}
    gravity_mars = 0.38  # 화성의 중력
    radius = diameter / 2
    pi_approximation = 3.14159 
    surface_area = 2 * pi_approximation * radius ** 2
    dome_area = surface_area / 2
    # 재료의 부피 계산 (면적 * 두께)
    volume = dome_area * thickness
    
    # 재료의 무게 계산 (부피 * 밀도 * 화성 중력)
    weight = volume * density[material] * gravity_mars
    
    # 결과를 소수점 이하 세 자리까지만 반올림하여 전역 변수에 저장
    total_dome_area = round(dome_area, 3)
    dome_weight = round(weight, 3)

# 사용자로부터 지름과 재질 입력 받기
diameter = float(input("반구체의 지름을 입력하세요 (단위: cm): "))
material = input("재질을 입력하세요 (glass, aluminum, carbon 중 선택): ")

# 함수 호출
sphere_area(diameter, material)
print("전역변수 이름:", "total_dome_area, dome_weight")
print("재질: material")
print("지름:", diameter)
print("두께: 1")
print("면적:", total_dome_area)
print("무게:", dome_weight, "kg")
print("출력 형식:", "재질 ->", material, ", 지름 ->", diameter, ", 두께 -> 1, 면적 ->", total_dome_area, ", 무게 ->", dome_weight, "kg")

