import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QGridLayout, QPushButton, QLineEdit

class Calculator(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Calculator")
        self.setGeometry(100, 100, 300, 400)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)

        self.display = QLineEdit()
        self.display.setReadOnly(True)
        self.layout.addWidget(self.display)

        self.buttons = [
            ('AC', (0, 0)),
            ('+/-', (0, 1)),
            ('%', (0, 2)),
            ('/', (0, 3)),
            ('7', (1, 0)),
            ('8', (1, 1)),
            ('9', (1, 2)),
            ('x', (1, 3)),
            ('4', (2, 0)),
            ('5', (2, 1)),
            ('6', (2, 2)),
            ('-', (2, 3)),
            ('1', (3, 0)),
            ('2', (3, 1)),
            ('3', (3, 2)),
            ('+', (3, 3)),
            ('0', (4, 0, 1, 2)),
            ('.', (4, 2)),
            ('=', (4, 3))
        ]

        self.grid_layout = QGridLayout()
        self.layout.addLayout(self.grid_layout)

        for button_text, pos in self.buttons:
            button = QPushButton(button_text)
            button.clicked.connect(self.on_button_click)
            self.grid_layout.addWidget(button, *pos)

        self.setStyleSheet("background-color: black; color: white;")

    def on_button_click(self):
        clicked_button = self.sender()
        text = clicked_button.text()
        current_text = self.display.text()

        if text == 'AC':
            self.display.setText('')
        elif text == '=':
            try:
                result = str(eval(current_text))
                self.display.setText(result)
            except Exception as e:
                self.display.setText('Error')
        else:
            self.display.setText(current_text + text)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    calc = Calculator()
    calc.show()
    sys.exit(app.exec_())
