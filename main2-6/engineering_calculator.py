import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QGridLayout, QPushButton, QLineEdit
from PyQt5.QtGui import QFont
import math

class EngineeringCalculator(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Engineering Calculator")
        self.setGeometry(100, 100, 300, 400)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)

        self.display = QLineEdit()
        self.display.setReadOnly(True)
        self.layout.addWidget(self.display)

        self.memory = None  # 메모리 슬롯

        self.buttons = [
            ('(', (0, 0)), (')', (0, 1)), ('mc', (0, 2)), ('m+', (0, 3)), ('m-', (0, 4)),
            ('mr', (0, 5)), ('AC', (0, 6)), ('+/-', (0, 7)), ('%', (0, 8)), ('/', (0, 9)),
            
            ('2nd', (1, 0)), ('x2', (1, 1)), ('x3', (1, 2)), ('xy', (1, 3)), ('ex', (1, 4)),
            ('10x', (1, 5)), ('7', (1, 6)), ('8', (1, 7)), ('9', (1, 8)), ('*', (1, 9)),
            
            ('1/x', (2, 0)), ('2√x', (2, 1)), ('3√x', (2, 2)), ('y√x', (2, 3)), ('ln', (2, 4)),
            ('log10', (2, 5)), ('4', (2, 6)), ('5', (2, 7)), ('6', (2, 8)), ('-', (2, 9)),
            
            ('x!', (3, 0)), ('sin', (3, 1)), ('cos', (3, 2)), ('tan', (3, 3)), ('e', (3, 4)),
            ('EE', (3, 5)), ('1', (3, 6)), ('2', (3, 7)), ('3', (3, 8)), ('+', (3, 9)),
            
            ('Rad', (4, 0)), ('sinh', (4, 1)), ('cosh', (4, 2)), ('tanh', (4, 3)), ('π', (4, 4)),
            ('Rand', (4, 5)), ('0', (4, 6)), ('.', (4, 7)), ('=', (4, 8))
        ]

        self.grid_layout = QGridLayout()
        self.layout.addLayout(self.grid_layout)

        for button_text, pos in self.buttons:
            button = QPushButton(button_text)
            button.clicked.connect(lambda _, text=button_text: self.on_button_click(text))
            self.grid_layout.addWidget(button, *pos)

    def on_button_click(self, text):
        current_text = self.display.text()

        if text == 'AC':
            self.display.clear()
        elif text == 'mc':
            self.memory = None  # 메모리 슬롯 지우기
        elif text in ('+', '-', '*', '/'):
            self.display.setText(current_text + ' ' + text + ' ')
        elif text == '=':
            try:
                result = eval(self.display.text())
                result = round(result, 6)  # 소수점 6자리 이하로 반올림하여 출력
                result_str = str(result)
                self.display.setText(result_str)
            except Exception as e:
                self.display.setText('Error')
        elif text == 'sin':
            try:
                angle = eval(current_text)
                result = math.sin(math.radians(angle))
                self.display.setText(str(result))
            except Exception as e:
                self.display.setText('Error')
        elif text == 'cos':
            try:
                angle = eval(current_text)
                result = math.cos(math.radians(angle))
                self.display.setText(str(result))
            except Exception as e:
                self.display.setText('Error')
        elif text == 'tan':
            try:
                angle = eval(current_text)
                result = math.tan(math.radians(angle))
                self.display.setText(str(result))
            except Exception as e:
                self.display.setText('Error')
        elif text == 'sinh':
            try:
                value = eval(current_text)
                result = math.sinh(value)
                self.display.setText(str(result))
            except Exception as e:
                self.display.setText('Error')
        elif text == 'cosh':
            try:
                value = eval(current_text)
                result = math.cosh(value)
                self.display.setText(str(result))
            except Exception as e:
                self.display.setText('Error')
        elif text == 'tanh':
            try:
                value = eval(current_text)
                result = math.tanh(value)
                self.display.setText(str(result))
            except Exception as e:
                self.display.setText('Error')
        elif text == 'π':
            self.display.setText(current_text + str(math.pi))
        elif text == 'x2':
            try:
                value = eval(current_text)
                result = value ** 2
                self.display.setText(str(result))
            except Exception as e:
                self.display.setText('Error')
        elif text == 'x3':
            try:
                value = eval(current_text)
                result = value ** 3
                self.display.setText(str(result))
            except Exception as e:
                self.display.setText('Error')
        else:
            self.display.setText(current_text + text)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    calc = EngineeringCalculator()
    calc.show()
    sys.exit(app.exec_())
